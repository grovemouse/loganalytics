﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;

[System.Serializable]
public class LogData
{
    public long startDateTime;
    public string date;
    public string method;
    public string description;

    public static LogData CreateFromJSON(string json)
    {
        return JsonUtility.FromJson<LogData>(json);
    }

    public string ToJSON()
    {
        return JsonUtility.ToJson(this);
    }
}

public class LogDataBuff
{
    public long startDateTime;
    public string date;
    public string method;
    public DefaultDescription defaultDescription;
    public MetaOutResponseDescription metaOutResponce;
}

[System.Serializable]
public class DefaultDescription {
    public string text;

    public static DefaultDescription CreateFromJSON(string json)
    {
        return JsonUtility.FromJson<DefaultDescription>(json);
    }

    public string ToJSON()
    {
        return JsonUtility.ToJson(this);
    }
}

[System.Serializable]
public class MetaOutResponseDescription
{
    public string auto_response;
    public string rexsense_Question_Question;
    public SystemText systemText;
    public string rexsense_Emotion_Angry;
    public string rexsense_GenderAge;
    public string rexsense_Emotion_Neutral;
    public SpeakerParams speaker_params;
    public string face;
    public string motion;
    public string rexsense_Emotion_Sad;
    public string rexsense_GenderAge_Child;
    public string rexsense_Emotion_Happy;
    public string rexsense_Question_Statement;
    public string rexsense_GenderAge_Female;
    public string rexsense_GenderAge_Male;
    public string type;
    public string template_id;

    public static MetaOutResponseDescription CreateFromJSON(string json)
    {
        return JsonUtility.FromJson<MetaOutResponseDescription>(json);
    }

    public string ToJSON()
    {
        return JsonUtility.ToJson(this);
    }
}

[System.Serializable]
public class SpeechRecognitionResultDescription
{
    public SpeechrecResult speechrec_result;
    public string finishDate;
    public string playTime;
    public string isCompleteEnd;
    public string version;
    public string type;

    public static SpeechRecognitionResultDescription CreateFromJSON(string json)
    {
        return JsonUtility.FromJson<SpeechRecognitionResultDescription>(json);
    }

    public string ToJSON()
    {
        return JsonUtility.ToJson(this);
    }
}

[System.Serializable]
public class EndSequenceDescription
{
    public string startDate;
    public string finishDate;
    public string playTime;
    public string isCompleteEnd;
    public string isReleaseHandEnd;
    public string releaseHandCnt;

    public static EndSequenceDescription CreateFromJSON(string json)
    {
        return JsonUtility.FromJson<EndSequenceDescription>(json);
    }

    public string ToJSON()
    {
        return JsonUtility.ToJson(this);
    }
}

[System.Serializable]
public class SystemText
{
    public string utterance;
    public string expression;
}

[System.Serializable]
public class SpeakerParams
{
    public string speech_rate;
    public string expression;
    public string style_id;
    public string voice_type;
    public string pitch;
    public string intonation;
    public string power_rate;
    public string speaker_id;
}

[System.Serializable]
public class SpeechrecResult
{
    public string result_status;
    public Sentences[] sentences;
    public string domain_id;
    public string language;
    public string recognition_id;
}

[System.Serializable]
public class Sentences
{
    public string converter_result;
    public string voiceText;
    public double score;
    public Words[] words;
}

[System.Serializable]
public class Words
{
    public double start_time;
    public double score;
    public double end_time;
    public string label;
}

public class LogAnalytics : MonoBehaviour {

    public string AnalyticsFolder;
    public string LogFilesFolder;
    private Dictionary<string, LogDataBuff> logDatas;

    //private string guitxt = "";
    string gender = "";

    // Use this for initialization
    void Start () {
        loadLogData();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void loadLogData()
    {
        string voiceText="";

        DateTime dt = DateTime.Now;
        string fileDate = dt.ToString("yyyy-MM-dd.HH.mm.ss");

        //StringReader reader = new StringReader(TextLines);

        string logText;
        string logSpeechText;

        logText = "ユーザID,実行日時,Question,回答,性別,男性度,女性度,子供度,喜度,怒度,哀度,楽度,回答状態,疑問度";
        StreamWriter sw = new StreamWriter(AnalyticsFolder + "/Analytics" + "_" + fileDate + ".csv", true);// TextData.txtというファイルを新規で用意
        sw.WriteLine(logText);// ファイルに書き出したあと改行
        sw.Flush();// StreamWriterのバッファに書き出し残しがないか確認
        sw.Close();

        StreamWriter swResult = new StreamWriter(AnalyticsFolder + "/AkushukaiResult" + "_" + fileDate + ".csv", true);// TextData.txtというファイルを新規で用意
        logText = "ユーザID,実行日時,性別,プレイ時間,終了,痛いよ終了,痛いよ回数";
        swResult.WriteLine(logText);// ファイルに書き出したあと改行
        swResult.Flush();// StreamWriterのバッファに書き出し残しがないか確認
        swResult.Close();


        logSpeechText = "ユーザID,実行日時,convert_result,voiceText,score,|,index,start_time,end_time,label,score,....";
        StreamWriter swSpeech = new StreamWriter(AnalyticsFolder + "/AnalyticsSpeechRec" + "_" + fileDate + ".csv", true);// TextData.txtというファイルを新規で用意
        swSpeech.WriteLine(logSpeechText);// ファイルに書き出したあと改行
        swSpeech.Flush();// StreamWriterのバッファに書き出し残しがないか確認
        swSpeech.Close();

        //"C:\test"以下の".txt"ファイルをすべて取得する
        DirectoryInfo di = new System.IO.DirectoryInfo(LogFilesFolder);
        FileInfo[] files = di.GetFiles("*.txt", System.IO.SearchOption.TopDirectoryOnly);

        //ListBox1に結果を表示する
        foreach (FileInfo f in files)
        {
            Debug.Log(f.FullName + "開始");
            StreamReader sr = new StreamReader(f.FullName, Encoding.GetEncoding("UTF-8"));

            while (sr.Peek() != -1)
            {
                logText = "";
                //voiceText = "";

                //Debug.Log(sr.ReadLine());
                string line = sr.ReadLine(); // 一行ずつ読み込み

                int descriptionIndex = line.IndexOf(",\"description\":", StringComparison.Ordinal);
                string descriptionText = line.Substring(descriptionIndex + ",\"description\":".Length);

                descriptionText = descriptionText.Replace("\"", "\\\"");
                descriptionText = descriptionText.Replace("}}", "}\"}");

                string jsonText = line.Substring(0, descriptionIndex);
                jsonText = jsonText + ",\"description\":\"" + descriptionText;

                LogData logData = LogData.CreateFromJSON(jsonText);

                if (logData.method.Equals("onVoiceTextOut"))
                {
                    DefaultDescription dd = DefaultDescription.CreateFromJSON(logData.description);
                    //Debug.Log(logData.method + " : " + dd.text);

                    voiceText = dd.text;
                }

                if (logData.method.Equals("onMetaOut"))
                {
                    //発話情報のログ
                    if(logData.description.IndexOf("nlu_result", StringComparison.Ordinal) > 0)
                    {
                        logText = convertToMetaOutTypeNulResult(voiceText, logData);
                        //Debug.Log(logText);
                    }
                    //発話情報解析ログ
                    if (logData.description.IndexOf("speech_recognition_result", StringComparison.Ordinal) > 0)
                    {
                        logSpeechText = convertToMetaOutTypeSpeechRec(logData);

                        swSpeech = new StreamWriter(AnalyticsFolder + "/AnalyticsSpeechRec" + "_" + fileDate + ".csv", true);// TextData.txtというファイルを新規で用意
                        swSpeech.WriteLine(logSpeechText);// ファイルに書き出したあと改行
                        swSpeech.Flush();// StreamWriterのバッファに書き出し残しがないか確認
                        swSpeech.Close();
                    }
                }

                /*　終了ログ */
                if (logData.method.Equals("endSequence"))
                {
                    EndSequenceDescription eqd = EndSequenceDescription.CreateFromJSON(logData.description);
                    logText = logData.startDateTime + "," + logData.date + ",";
                    logText = logText + gender + "," + eqd.playTime + "," + eqd.isCompleteEnd + "," + eqd.isReleaseHandEnd + "," + eqd.releaseHandCnt;

                    swResult = new StreamWriter(AnalyticsFolder + "/AkushukaiResult" + "_" + fileDate + ".csv", true);// TextData.txtというファイルを新規で用意
                    swResult.WriteLine(logText);// ファイルに書き出したあと改行
                    swResult.Flush();// StreamWriterのバッファに書き出し残しがないか確認
                    swResult.Close();

                    gender = "";
                    logText = "";
                }
                //Debug.Log(TextLines);
                if (!logText.Equals(""))
                {
                    sw = new StreamWriter(AnalyticsFolder + "/Analytics" + "_" + fileDate + ".csv", true);// TextData.txtというファイルを新規で用意
                    sw.WriteLine(logText);// ファイルに書き出したあと改行
                    sw.Flush();// StreamWriterのバッファに書き出し残しがないか確認
                    sw.Close();
                }
            }
            sr.Close();
        }
        Debug.Log("終了！！！！");
    }

    /*
    * 発話情報ログ出力のための関数  
     */  
    private string convertToMetaOutTypeNulResult(string voiceText ,LogData logData)
    {
        string title="";
        string log = "";

        MetaOutResponseDescription mord = MetaOutResponseDescription.CreateFromJSON(logData.description);
        if (logData.description.IndexOf("rexsense", StringComparison.Ordinal) > 0)
        {
            switch (mord.template_id)
            {
                case "#003-1":
                    title = "名前";
                    //title = mord.auto_response;
                    log = logData.startDateTime + "," + logData.date + ",";
                    log = log + title + "," + voiceText + ",";
                    log = log + mord.rexsense_GenderAge + ",";
                    log = log + mord.rexsense_GenderAge_Male + ",";
                    log = log + mord.rexsense_GenderAge_Child + ",";
                    log = log + mord.rexsense_GenderAge_Female + ",";
                    log = log + mord.rexsense_Emotion_Happy + ",";
                    log = log + mord.rexsense_Emotion_Angry + ",";
                    log = log + mord.rexsense_Emotion_Sad + ",";
                    log = log + mord.rexsense_Emotion_Neutral + ",";
                    log = log + mord.rexsense_Question_Statement + ",";
                    log = log + mord.rexsense_Question_Question;
                    break;
                case "#006-1":
                    title = "場所";
                    //title = mord.auto_response;
                    log = logData.startDateTime + "," + logData.date + ",";
                    log = log + title + "," + voiceText + ",";
                    log = log + mord.rexsense_GenderAge + ",";
                    log = log + mord.rexsense_GenderAge_Male + ",";
                    log = log + mord.rexsense_GenderAge_Child + ",";
                    log = log + mord.rexsense_GenderAge_Female + ",";
                    log = log + mord.rexsense_Emotion_Happy + ",";
                    log = log + mord.rexsense_Emotion_Angry + ",";
                    log = log + mord.rexsense_Emotion_Sad + ",";
                    log = log + mord.rexsense_Emotion_Neutral + ",";
                    log = log + mord.rexsense_Question_Statement + ",";
                    log = log + mord.rexsense_Question_Question;
                    break;
                case "#009-1":
                    title = "好き嫌い";
                    //title = mord.auto_response;
                    log = logData.startDateTime + "," + logData.date + ",";
                    log = log + title + "," + voiceText + ",";
                    log = log + mord.rexsense_GenderAge + ",";
                    log = log + mord.rexsense_GenderAge_Male + ",";
                    log = log + mord.rexsense_GenderAge_Child + ",";
                    log = log + mord.rexsense_GenderAge_Female + ",";
                    log = log + mord.rexsense_Emotion_Happy + ",";
                    log = log + mord.rexsense_Emotion_Angry + ",";
                    log = log + mord.rexsense_Emotion_Sad + ",";
                    log = log + mord.rexsense_Emotion_Neutral + ",";
                    log = log + mord.rexsense_Question_Statement + ",";
                    log = log + mord.rexsense_Question_Question;
                    break;
                case "#009-1-1":
                    title = "好き嫌い";
                    //title = mord.auto_response;
                    log = logData.startDateTime + "," + logData.date + ",";
                    log = log + title + "," + voiceText + ",";
                    log = log + mord.rexsense_GenderAge + ",";
                    log = log + mord.rexsense_GenderAge_Male + ",";
                    log = log + mord.rexsense_GenderAge_Child + ",";
                    log = log + mord.rexsense_GenderAge_Female + ",";
                    log = log + mord.rexsense_Emotion_Happy + ",";
                    log = log + mord.rexsense_Emotion_Angry + ",";
                    log = log + mord.rexsense_Emotion_Sad + ",";
                    log = log + mord.rexsense_Emotion_Neutral + ",";
                    log = log + mord.rexsense_Question_Statement + ",";
                    log = log + mord.rexsense_Question_Question;
                    break;
                case "#008-1":
                    title = "好き嫌い";
                    //title = mord.auto_response;
                    log = logData.startDateTime + "," + logData.date + ",";
                    log = log + title + "," + voiceText + ",";
                    log = log + mord.rexsense_GenderAge + ",";
                    log = log + mord.rexsense_GenderAge_Male + ",";
                    log = log + mord.rexsense_GenderAge_Child + ",";
                    log = log + mord.rexsense_GenderAge_Female + ",";
                    log = log + mord.rexsense_Emotion_Happy + ",";
                    log = log + mord.rexsense_Emotion_Angry + ",";
                    log = log + mord.rexsense_Emotion_Sad + ",";
                    log = log + mord.rexsense_Emotion_Neutral + ",";
                    log = log + mord.rexsense_Question_Statement + ",";
                    log = log + mord.rexsense_Question_Question;
                    break;
                case "#008-1-1":
                    title = "好き嫌い";
                    //title = mord.auto_response;
                    log = logData.startDateTime + "," + logData.date + ",";
                    log = log + title + "," + voiceText + ",";
                    log = log + mord.rexsense_GenderAge + ",";
                    log = log + mord.rexsense_GenderAge_Male + ",";
                    log = log + mord.rexsense_GenderAge_Child + ",";
                    log = log + mord.rexsense_GenderAge_Female + ",";
                    log = log + mord.rexsense_Emotion_Happy + ",";
                    log = log + mord.rexsense_Emotion_Angry + ",";
                    log = log + mord.rexsense_Emotion_Sad + ",";
                    log = log + mord.rexsense_Emotion_Neutral + ",";
                    log = log + mord.rexsense_Question_Statement + ",";
                    log = log + mord.rexsense_Question_Question;
                    break;
            }
            //Debug.Log(logData.method + " : " + logText);
            //性別書き込み（広域変数なので注意）
            if (gender.Equals(""))
            {
                gender = mord.rexsense_GenderAge;   
            }
        }

        //Debug.Log(TextLines);
        if (!log.Equals(""))
        {
            Debug.Log(log);
        }

        return log;
    }

    private string convertToMetaOutTypeSpeechRec(LogData logData)
    {
        string log = "";
        int wordIndex = 1;

        SpeechRecognitionResultDescription spc = SpeechRecognitionResultDescription.CreateFromJSON(logData.description);

        foreach(Sentences sentnce in spc.speechrec_result.sentences){
            log = log + logData.startDateTime + "," + logData.date + ",";
            log = log + sentnce.converter_result + ",";
            log = log + sentnce.voiceText + ",";
            log = log + sentnce.score + ",";
            log = log + "|" + ",";

            wordIndex = 1;
            foreach (Words word in sentnce.words)
            {
                log = log + wordIndex.ToString() + "->,";
                log = log + word.start_time + ",";
                log = log + word.end_time + ",";
                log = log + word.label + ",";
                log = log + word.score + ",";

                wordIndex = wordIndex + 1;
            }
            log = log.Substring(0, log.Length - 1);
            log = log + "\r\n";
        }
        log = log.Substring(0, log.Length - "\r\n".Length);

        //Debug.Log(logText);

        return log;
    }
}